<?php
/**
 *
 * @package   ZWC Woocommece tools
 * @author    Vasyl Halushchak @ Grafica Isernina <vasylware@gmail.com>
 * @license   GPL-2.0+
 * @copyright 2020 Vasyl Halushchak
 *
 * @wordpress-plugin
 * Plugin Name:			ZWC Woocommece tools
 * Version:           	1.1.0
 * Author:       			Vasyl Halushchak @ Grafica Isernina
 * Text Domain:       	zwct
 * License:           	GPL-2.0+
 * License URI:       	http://www.gnu.org/licenses/gpl-2.0.txt
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}
if ( is_admin() && class_exists( 'woocommerce' ) ) {

	function zwcoa_get_total_sum($view){

		global $wpdb;

		return number_format(apply_filters(
			'woocommerce_reports_sales_overview_order_totals',
			$wpdb->get_var( "
				SELECT SUM(meta.meta_value) AS total_sales FROM {$wpdb->posts} AS posts
				LEFT JOIN {$wpdb->postmeta} AS meta ON posts.ID = meta.post_id
				WHERE meta.meta_key = '_order_total'
				AND posts.post_type = 'shop_order'
				AND posts.post_status = '{$view}'
			")));
	}

	add_action( 'pre_get_posts', function( $query ) {

		add_filter('views_edit-shop_order', function ( $views ) {

			$post_type = get_query_var('post_type');

			if ($post_type == 'shop_order') {

				foreach ($views as $key => $view_name) {
					switch ($key) {
						case 'wc-pending':
						case 'wc-processing':
						case 'wc-on-hold':
						case 'wc-completed':
						case 'wc-refunded':
								$views[$key] = substr($view_name, 0, -12) . ' ~ €' . zwcoa_get_total_sum($key) . ')</span></a>';
							break;
					}
				}
			}

			return $views;
		} );

	} );

	add_filter('woocommerce_admin_order_data_after_billing_address', function ($order) {
		?>Tel: <a href="tel:<?php echo $order->get_billing_phone(); ?>"><?php echo $order->get_billing_phone(); ?></a> <?php
		$alt = get_user_meta($order->get_customer_id(), 'billing_phone', true);
		if ($alt !== $order->get_billing_phone() ) {
			?><br>Tel account: <a href="tel:<?php echo $alt; ?>"><?php echo $alt; ?></a> <?php
		}
	}, 10, 1);


	add_action('admin_enqueue_scripts', function() {
		wp_enqueue_style('zwctools-admin', plugin_dir_url( __FILE__ ) . 'admin.css');
	});

	add_action('parse_query', function($query) {
		global $typenow;
		if( is_admin() && $query->is_main_query() && $typenow == 'shop_order' ){
			if($query->get('orderby') == 'date'){
				$query->set('orderby', 'ID');
			}
		}
	});

}